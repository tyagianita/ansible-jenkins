---
# Variable setup.
- name: Change hostname
  hostname: name=jenkins 


# Jenkins installation
- name: Define jenkins_repo_url
  set_fact:
    jenkins_repo_url: "{{ __jenkins_repo_url }}"
  when: jenkins_repo_url is not defined

- name: Define jenkins_repo_key_url
  set_fact:
    jenkins_repo_key_url: "{{ __jenkins_repo_key_url }}"
  when: jenkins_repo_key_url is not defined

- name: Define jenkins_pkg_url
  set_fact:
    jenkins_pkg_url: "{{ __jenkins_pkg_url }}"
  when: jenkins_pkg_url is not defined

# Setup/install tasks.
- name: Ensure dependencies are installed.
  apt: name=curl state=installed

#- name: Install git
#  apt: name=git state=installed

- name: Install python-jenkins
  apt: name=python-jenkins state=installed

- name: Install python-lxml
  apt: name=python-lxml state=installed 

- name: Add Jenkins apt repository key.
  apt_key: url={{ jenkins_repo_key_url }} state=present

- name: Add Jenkins apt repository.
  apt_repository: repo={{ jenkins_repo_url }} state=present update_cache=yes

- name: Download specific Jenkins version.
  get_url:
    url: "{{ jenkins_pkg_url }}/jenkins_{{ jenkins_version }}_all.deb"
    dest: "/tmp/jenkins.deb"
  when: jenkins_version is defined

- name: Check if we downloaded a specific version of Jenkins.
  stat: path="/tmp/jenkins.deb"
  register: specific_version

- name: Install our specific version of Jenkins.
  apt:
    deb: "/tmp/jenkins.deb"
    state: installed
  when: specific_version.stat.exists

- name: Validate Jenkins is installed and register package name.
  apt: name=jenkins state=present
  notify: configure default users



# Configure Jenkins init settings.
- include: settings.yml
- template:
    src: basic-security.groovy
    dest: /var/lib/jenkins/init.groovy.d/basic-security.groovy

- name: Restart Jenkins to make the plugin data available
  service: name=jenkins state=restarted
# Make sure Jenkins starts, then configure Jenkins.

- name: Wait for Jenkins to start up before proceeding.
  shell: "curl -D - --silent --max-time 5 http://{{ jenkins_hostname }}:{{ jenkins_http_port }}{{ jenkins_url_prefix }}/cli/"
  register: result
  until: (result.stdout.find("403 Forbidden") != -1) or (result.stdout.find("200 OK") != -1) and (result.stdout.find("Please wait while") == -1)
  retries: "{{ jenkins_connection_retries }}"
  delay: "{{ jenkins_connection_delay }}"
  changed_when: false

- name: Get the jenkins-cli jarfile from the Jenkins server.
  get_url:
    url: "http://{{ jenkins_hostname }}:{{ jenkins_http_port }}{{ jenkins_url_prefix }}/jnlpJars/jenkins-cli.jar"
    dest: "{{ jenkins_jar_location }}"
  register: jarfile_get
  until: "'OK' in jarfile_get.msg or 'file already exists' in jarfile_get.msg"
  retries: 5
  delay: 10

#- name: Remove Jenkins security init scripts after first startup.
#  file:
#    path: "{{ jenkins_home }}/init.groovy.d/basic-security.groovy"
#    state: absent

# Update Jenkins and install configured plugins.
- include: plugins.yml


# Create seed job
- template:
    src: create-seed-job.groovy.j2
    dest: /var/lib/jenkins/init.groovy.d/create-seed-job.groovy

- name: Restart Jenkins to make the plugin data available
  service: name=jenkins state=restarted

- template:
    src: org.jenkinsci.plugins.ghprb.GhprbTrigger.xml.j2
    dest: /var/lib/jenkins/org.jenkinsci.plugins.ghprb.GhprbTrigger.xml

- template:
    src: hudson.plugins.groovy.Groovy.xml.j2
    dest: /var/lib/jenkins/hudson.plugins.groovy.Groovy.xml


# Create jenkins credentials
#- template:
#    src: credentials.sh.j2
#    dest: /tmp/credentials.sh
#    mode: 0755

#- command: sh /tmp/credentials.sh

# Maven installation
- template:
    src: hudson.tasks.Maven.xml.j2
    dest: /var/lib/jenkins/hudson.tasks.Maven.xml

# Create directory 
- name: Creates directory
  file: path=/var/lib/jenkins/workspace/newSeedJob/config state=directory
- name: create directory
  file: path=/var/lib/jenkins/.ssh

# Sonar configuration
- template:
    src: global_configuration.yml.j2
    dest: /var/lib/jenkins/workspace/newSeedJob/config/global_configuration.yml


# Copy files
- copy: src=/home/vagrant/rajiv/id_rsa dest=/var/lib/jenkins/.ssh/id_rsa
  failed_when: False
- name: copy key
  copy:
   src: id_rsa
   dest: /var/lib/jenkins/.ssh/id_rsa
   force: no

